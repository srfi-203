;;;; SPDX-FileCopyrightText: 2021 Vasilij Schneidermann <mail@vasilij.de>
;;;;
;;;; SPDX-License-Identifier: BSD-3-Clause

(module srfi-203
    (canvas-path canvas-width canvas-height canvas-frame
     canvas-reset canvas-refresh canvas-cleanup
     draw-line
     rogers
     jpeg-file->painter
     image-file->painter)

  (import scheme)
  (import (chicken base))
  (import (chicken file))
  (import (chicken format))
  (import (chicken platform))
  (import (chicken string))

  (register-feature! 'srfi-203)

  (include "srfi-203.scm")
  (include "rogers.scm"))
