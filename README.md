<!--
SPDX-FileCopyrightText: 2021 Vasilij Schneidermann <mail@vasilij.de>

SPDX-License-Identifier: BSD-3-Clause
-->

## About

Implementation of
[SRFI-203](https://srfi.schemers.org/srfi-203/srfi-203.html) (A Simple
Picture Language in the Style of SICP) using the former [sicp
egg](http://wiki.call-cc.org/eggref/4/sicp) for CHICKEN.

## Docs

See [its wiki page].

[its wiki page]: https://wiki.call-cc.org/eggref/5/srfi-203
